<?php
session_start();
?>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="style.css" />
  <title>Hostname</title>
</head>
<body>
  <?php
  if (!empty($_SESSION['logged_in']))
  {
    $hostname = htmlspecialchars($_POST['hostname']);
    $hostname = trim($hostname);
    $hostname_file = fopen('/etc/hostname', 'a+'); // il faut donner les droits en écriture sur /etc/hostname pour que cela fonctionne
    ftruncate($hostname_file,0);
    fputs($hostname_file,$hostname);
    fclose($hostname_file);
    echo '<div class="form"><div class="text-center">Le nouveau nom du serveur est '.$hostname.'</div></div>';
    ?>
    <div class="container">
      <div class="row">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Manage Device</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="websvn/index.php"><span class="glyphicon glyphicon-list"></span> Websvn</a></li>
                <li><a href="settings.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
                <li><a href="logout.php"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="text-center">Un reboot est nécessaire !</div>
    <div class="text-center">
      <button type="button" class="btn btn-default btn-sm" onclick="location.href='index.php'">
        <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
      </button>
    </div>
    <footer>
      <div class="container">
        <div class="row">
          <p>&copy; 2017 B2M SAS</p>
        </div>
      </div>
    </div>
  </footer>
  <?php
}
else
{
  header('Location:login.php');
}
?>
</body>
</html>
