<?php
// Modification du mdp admin
require ('fonction.php');
session_start();
  if (!empty($_SESSION['logged_in']))
  {
    $password1 = htmlspecialchars($_POST['password1']);
    $password2 = htmlspecialchars($_POST['password2']);

    if ($password1 === $password2) // vérification des password identiques
    {
      $hashed_password = sha1($password1);
      bdConnection();
      $R = $GLOBALS['bd']->query("UPDATE user SET password = '$hashed_password' WHERE username = 'admin'"); // Remplacement du mdp dans la base
      header('Location:settings.php');
    }
    else {
      header('Location:erreur.php');
    }
  }
  else
  {
    header('Location:login.php');
  }
  ?>
