-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le :  jeu. 25 juil. 2019 à 00:19
-- Version du serveur :  5.7.27
-- Version de PHP :  7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `oxidized`
--

-- --------------------------------------------------------

--
-- Structure de la table `devices`
--

CREATE TABLE `devices` (
  `ID` int(11) NOT NULL,
  `ip_address` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `username` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `auto_enable` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `password_enable` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `connection` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `modele` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `modeles`
--

CREATE TABLE `modeles` (
  `ID` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `modeles`
--

INSERT INTO `modeles` (`ID`, `name`) VALUES
(1, 'A10 Networks'),
(2, 'Accedian Performance Elements'),
(3, 'Adtran'),
(4, 'Alcatel-Lucent'),
(5, 'Allied Telesis'),
(6, 'Alvarion'),
(7, 'APC'),
(8, 'Arista'),
(9, 'Arris'),
(10, 'Aruba'),
(11, 'AudioCodes'),
(12, 'Avaya'),
(13, 'Brocade'),
(14, 'Calix'),
(15, 'Cambium'),
(16, 'Casa'),
(17, 'Check Point'),
(18, 'Ciena'),
(19, 'Cisco'),
(20, 'Citrix'),
(21, 'Coriant'),
(22, 'ComNet'),
(23, 'Comtrol'),
(24, 'Cumulus'),
(25, 'DataCom'),
(26, 'DCN'),
(27, 'DELL'),
(28, 'D-Link'),
(29, 'ECI Telecom'),
(30, 'EdgeCore'),
(31, 'Ericsson/Redback'),
(32, 'Extreme Networks'),
(33, 'F5'),
(34, 'Fiberstore'),
(35, 'Firebrick'),
(36, 'Force10'),
(37, 'FortiGate'),
(38, 'Fujitsu'),
(39, 'GCOM Technologies'),
(40, 'Grandstream Networks'),
(41, 'Hatteras'),
(42, 'Hillstone Networks'),
(43, 'Hirschmann'),
(44, 'HP'),
(45, 'Huawei'),
(46, 'Icotera'),
(47, 'Juniper'),
(48, 'Mellanox'),
(49, 'Mikrotik'),
(50, 'Motorola'),
(51, 'MRV'),
(52, 'Netgear'),
(53, 'Netonix'),
(54, 'Nokia'),
(55, 'OneAccess'),
(56, 'Opengear'),
(57, 'Palo Alto'),
(58, 'Pure Storage'),
(59, 'Radware'),
(60, 'Raisecom'),
(61, 'Quanta'),
(62, 'Siklu'),
(63, 'SonicWALL'),
(64, 'SNR'),
(65, 'Supermicro'),
(66, 'Symantec'),
(67, 'Trango Systems'),
(68, 'TPLink'),
(69, 'Ubiquiti'),
(70, 'Watchguard'),
(71, 'Westell'),
(72, 'Zhone'),
(73, 'Zyxel');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `username` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`ID`, `username`, `password`) VALUES
(1, 'admin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `devices`
--
ALTER TABLE `devices`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Index pour la table `modeles`
--
ALTER TABLE `modeles`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `devices`
--
ALTER TABLE `devices`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `modeles`
--
ALTER TABLE `modeles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
