<?php
require ('fonction.php');
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="style.css" />
  <script src="jquery-3.2.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <title>Manage Devices</title>
</head>
<body>
  <?php
  if (!empty($_SESSION['logged_in']))
  {
    ?>
<div class="container">
  <div class="row">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
          <a class="navbar-brand" href="index.php">Manage Devices</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <!-- <li><a href="websvn/index.php"><span class="glyphicon glyphicon-list"></span> Websvn</a></li> -->
          <li><a href="settings.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
          <li><a href="logout.php"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
        </ul>
      </div>
    </div>
    </nav>
  </div>
</div>
<div class="container">
  <form action="add_device.php" method="post" name="add_device" class="form">  <!-- Formulaire ajout d'un device -->
    <legend>Ajouter un équipement</legend>
    <div class="row">
      <div class="col-md-offset-1 col-md-4">
        <div class="form-group">
          <label class="control-label" for="ip_address">Adresse IP <em>*</em></label>
          <input type="text" class="form-control" name="ip_address" placeholder="192.168.0.0" autofocus="" required=""/>
        </div>
      </div>
      <div class="col-md-offset-2 col-md-4">
        <div class="form-group">
          <label class="control-label" for="name">Nom de l'équipement <em>*</em></label>
          <input type="text" class="form-control" name="name" placeholder="Device Name" required=""/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-offset-1 col-md-4">
        <div class="form-group">
          <label class="control-label" for="authentication">Authentification <em>*</em></label>
          <input type="text" class="form-control" name="username_device" placeholder="Username" required=""/>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-offset-2 col-md-4">
          <label class="control-label" for="password">Mot de passe ssh/telnet <em>*</em></label>
          <input type="password" class="form-control" name="password_device" placeholder="Password" required=""/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-offset-1 col-md-4">
        <div class="form-group">
          <label for="auto_enable">Auto-enable :</label>
          <input type="radio" class="form-control radio-inline" name="auto_enable" value="oui" id="oui" checked="checked" onclick="passwordEnable()"/>
          <label for="oui" class="text-radio">Oui</label>
          <input type="radio" class="form-control radio-inline" name="auto_enable" value="non" id="non" onclick="passwordEnable()"/>
          <label for="non" class="text-radio">Non</label>
        </div>
      </div>
      <div class="col-md-offset-2 col-md-4">
        <div id="password_enable" class="form-group">
          <label class="control-label" for="password_enable">Mot de passe enable <em>*</em></label>
          <input id="disabledInput" class="form-control" type="password" name="password_enable" placeholder="Password" required="" disabled/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-offset-1 col-md-4">
        <div class="form-group">
          <label for="connection">Connectivité :</label>
          <input type="radio" class="form-control radio-inline" name="connection" value="ssh" id="ssh" checked="checked"/>
          <label for="ssh" class="text-radio">SSH</label>
          <input type="radio" class="form-control radio-inline" name="connection" value="telnet" id="telnet"/>
          <label for="telnet" class="text-radio">Telnet</label>
        </div>
      </div>
      <div class="col-md-offset-2 col-md-4">
        <div class="form-group">
          <label class="control-label" for="modele">Modèle d'équipement</label>
          <select name="modele" class="form-control"> <!-- Select dynamique -->
            <?php
            bdConnection();
            $R = $GLOBALS['bd']->query("SELECT * FROM modeles");
            while ($donnees = $R->fetch())
            {
               echo '<option value="'.$donnees['name'].'">'.$donnees['name'].'</option>';
            }
            ?>
          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="form-group">
          <button type="submit" class="btn btn-default btn-sm center-block">
            <span class="glyphicon glyphicon-plus"></span> Ajouter
          </button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Lancement du script de mise à jour -->
<?php
    if (isset($_POST['update']))
    {
	 shell_exec("sudo service oxidized restart"); // Donner les droits sudo à l'utilisateur www-data
    }
?>
<div class="container">
  <form method="post">
    <div class="col-md-offset-4 col-md-4">
    <button type="submit" name="update" class="btn btn-default btn-sm center-block">
      <span class="glyphicon glyphicon-refresh"></span> Update
    </button>
  </div>
</form>
</div>
<div class="container">
  <div class="row">
      <legend>Liste des équipements</legend>
      <div class="panel panel-default">
        <table class="table table-fixed table-striped">
          <thead>
            <tr>
                <th class="col-xs-2">Nom équipement </th>
                <th class="col-xs-2">Adresse IP</th>
                <th class="col-xs-2">Auto-enable</th>
                <th class="col-xs-2">Connection</th>
                <th class="col-xs-2">Modèle</th>
                <th class="col-xs-1">Supprimer</th>
                <th class="col-xs-1">Modifier</th>
            </tr>
          </thead>
          <tbody>
          <?php 
          bdConnection();
          $R = $GLOBALS['bd']->query("SELECT * FROM devices");
          while ($donnees = $R->fetch()){ 
            ?>
            <tr>
              <td class="col-xs-2"><?php echo $donnees['name'];?></td>
              <td class="col-xs-2"><?php echo $donnees['ip_address'];?></td>
              <td class="col-xs-2"><?php echo $donnees['auto_enable'];?></td>
              <td class="col-xs-2"><?php echo $donnees['connection'];?></td>
              <td class="col-xs-2"><?php echo $donnees['modele'];?></td>
              <td class="col-xs-1"><form action='delete_device.php' method="post" name="delete_device">
                 <input type="hidden" name="name_del" value="<?php echo $donnees['name']; ?>">
                 <!-- <button type="button" class="btn btn-default" Onclick="deleteDevice()">
                  <span class="glyphicon glyphicon-trash"></span>
                </button> -->
                <button type="submit" class="btn btn-default">
                  <span class="glyphicon glyphicon-trash"></span>
                </button>
              </form></td>
              <td class="col-xs-1"><form action='edit_device.php' method="post" name="edit_device">
                 <input type="hidden" name="id" value="<?php echo $donnees['ID']; ?>">
                 <button type="submit" class="btn btn-default">
                  <span class="glyphicon glyphicon-edit"></span>
                </button>
              </form></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
  </div>
</div>
<footer>
    <div class="container">
        <div class="row">
              <p>&copy; 2017 B2M SAS</p>
          </div>
        </div>
</footer>
<?php
}
else
{
 header('Location:login.php');
}
?>


  <script src="code.js"></script>
</body>
</html>
